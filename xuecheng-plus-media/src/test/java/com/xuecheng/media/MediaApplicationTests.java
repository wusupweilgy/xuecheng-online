package com.xuecheng.media;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
class MediaApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void breakpointContinuationTest() throws Exception {

        //读取原始文件
        File file = new File("C:\\Users\\26731\\Desktop\\屏幕截图 2023-02-13 171333.jpg");

        //设置分块大小
        int size = 1024 * 1024;
        byte[] bytes = new byte[size];

        //读取文件

            RandomAccessFile raf_read = new RandomAccessFile(file, "r");
            File file1 = new File("C:\\Users\\26731\\Desktop\\lgy.jpg");
            if (file1.exists())
                file1.delete();
            boolean newFile = file1.createNewFile();
            if (newFile) {
                //向分块文件中写数据
                RandomAccessFile raf_write = new RandomAccessFile(file1, "rw");
                int len = -1;
                while ((len = raf_read.read(bytes)) != -1) {
                    raf_write.write(bytes, 0, len);
                }
                raf_write.close();
                System.out.println("文件写出成功");
            }

            raf_read.close();

    }

    @Test
    void merage() throws IOException {
//块文件目录
        File chunkFolder = new File("C:\\Users\\26731\\Desktop\\chunk");
        //原始文件
        File originalFile = new File("C:\\Users\\26731\\Desktop\\lgy.jpg");
        //合并文件
        File mergeFile = new File("C:\\Users\\26731\\Desktop\\lgy2.jpg");
        if (mergeFile.exists()) {
            mergeFile.delete();
        }
        //创建新的合并文件
        mergeFile.createNewFile();
        //用于写文件
        RandomAccessFile raf_write = new RandomAccessFile(mergeFile, "rw");
        //指针指向文件顶端
        raf_write.seek(0);
        //缓冲区
        byte[] b = new byte[1024];
        //分块列表
        File[] fileArray = chunkFolder.listFiles();
        // 转成集合，便于排序
        List<File> fileList = Arrays.asList(fileArray);
        // 从小到大排序
        Collections.sort(fileList, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return Integer.parseInt(o1.getName()) - Integer.parseInt(o2.getName());
            }
        });
        //合并文件
        for (File chunkFile : fileList) {
            RandomAccessFile raf_read = new RandomAccessFile(chunkFile, "rw");
            int len = -1;
            while ((len = raf_read.read(b)) != -1) {
                raf_write.write(b, 0, len);

            }
            raf_read.close();
        }
        raf_write.close();

        //校验文件
        try (

                FileInputStream fileInputStream = new FileInputStream(originalFile);
                FileInputStream mergeFileStream = new FileInputStream(mergeFile);


        ) {
            //取出原始文件的md5
            String originalMd5 = DigestUtils.md5Hex(fileInputStream);
            //取出合并文件的md5进行比较
            String mergeFileMd5 = DigestUtils.md5Hex(mergeFileStream);
            if (originalMd5.equals(mergeFileMd5)) {
                System.out.println("合并文件成功");
            } else {
                System.out.println("合并文件失败");
            }

        }


    }

    @Test
    void test() throws IOException {
        File file = new File("C:\\Users\\26731\\Desktop\\lgy.jpg");
        File file1 = new File("C:\\Users\\26731\\Desktop\\ll.jpg");
        IOUtils.copy(new FileInputStream(file),new FileOutputStream(file1));

    }
}