package com.xuecheng.media.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.base.model.PageParams;
import com.xuecheng.base.model.PageResult;
import com.xuecheng.base.model.RestResponse;
import com.xuecheng.media.model.dto.QueryMediaParamsDto;
import com.xuecheng.media.model.dto.UploadFileParamsDto;
import com.xuecheng.media.model.dto.UploadFileResultDto;
import com.xuecheng.media.model.po.MediaFiles;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

/**
 * <p>
 * 媒资信息 服务类
 * </p>
 *
 * @author itcast
 * @since 2023-02-13
 */
public interface MediaFilesService extends IService<MediaFiles> {


    /**
     * @description 上传文件
     * @param uploadFileParamsDto  上传文件信息
     * @param folder  文件目录,如果不传则默认年、月、日
     * @return com.xuecheng.media.model.dto.UploadFileResultDto 上传文件结果
     * @author Mr.M
     * @date 2022/9/12 19:31
     */
    public UploadFileResultDto uploadFile(Long companyId, UploadFileParamsDto uploadFileParamsDto, byte[] bytes, String folder, String objectName);

    /**
     * @description 查询文件列表
     * @param companyId 机构id
     * @param pageParams 页面参数
     * @param queryMediaParamsDto 查询文件参数dto
     * @return {@link PageResult }<{@link MediaFiles }>
     * @author LGY
     * @date 2023/02/13 18:57
     */
    PageResult<MediaFiles> queryMediaFiels(Long companyId, PageParams pageParams, QueryMediaParamsDto queryMediaParamsDto);


    /**
     * @description 检查文件是否存在
     * @param fileMd5 文件的md5
     * @return com.xuecheng.base.model.RestResponse<java.lang.Boolean> false不存在，true存在
     * @author Mr.M
     * @date 2022/9/13 15:38
     */
    public RestResponse<Boolean> checkFile(String fileMd5);

    /**
     * @description 检查分块是否存在
     * @param fileMd5  文件的md5
     * @param chunkIndex  分块序号
     * @return com.xuecheng.base.model.RestResponse<java.lang.Boolean> false不存在，true存在
     * @author Mr.M
     * @date 2022/9/13 15:39
     */
    public RestResponse<Boolean> checkChunk(String fileMd5, int chunkIndex);

    /**
     * @description 上传分块
     * @param fileMd5  文件md5
     * @param chunk  分块序号
     * @param bytes  文件字节
     * @return com.xuecheng.base.model.RestResponse
     * @author Mr.M
     * @date 2022/9/13 15:50
     */
    public RestResponse uploadChunk(String fileMd5, int chunk, byte[] bytes);


    /**
     * @description 合并分块
     * @param companyId  机构id
     * @param fileMd5  文件md5
     * @param chunkTotal 分块总和
     * @param uploadFileParamsDto 文件信息
     * @return com.xuecheng.base.model.RestResponse
     * @author Mr.M
     * @date 2022/9/13 15:56
     */
    public RestResponse mergechunks(Long companyId, String fileMd5, int chunkTotal, UploadFileParamsDto uploadFileParamsDto);

    /**
     * @param companyId
     * @param fileId
     * @param uploadFileParamsDto
     * @param bucket
     * @param objectName
     * @return com.xuecheng.media.model.po.MediaFiles
     * @description 将文件信息入库
     * @author Mr.M
     * @date 2022/10/14 9:14
     */
    @Transactional
    public MediaFiles addMediaFilesToDb(Long companyId, String fileId, UploadFileParamsDto uploadFileParamsDto, String bucket, String objectName);

    /**
     * @description 根据id查询文件信息
     * @param id  文件id
     * @return com.xuecheng.media.model.po.MediaFiles 文件信息
     * @author Mr.M
     * @date 2022/9/13 17:47
     */
    public MediaFiles getFileById(String id);

    /**
     * @description 从min-io下载文件
     * @param originalFile 原始文件
     * @param bucket 桶
     * @param filePath 文件路径
     *
     * @author LGY
     * @date 2023/02/16 16:09
     */
    public File downloadFileFromMinIO(File originalFile, String bucket, String filePath);

    /**
     * @description 将媒体文件添加到min-io
     * @param mp4_path mp4路径
     * @param bucket 桶
     * @param objectName 对象名称
     *
     * @author LGY
     * @date 2023/02/16 16:09
     */
    public void addMediaFilesToMinIO(String mp4_path, String bucket, String objectName);
}