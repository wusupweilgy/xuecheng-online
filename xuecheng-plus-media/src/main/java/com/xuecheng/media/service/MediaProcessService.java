package com.xuecheng.media.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.media.model.po.MediaProcess;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author itcast
 * @since 2023-02-13
 */
public interface MediaProcessService extends IService<MediaProcess> {

    /**
     * @description 将url存储至数据，并更新状态为成功，并将待处理视频记录删除存入历史
     * @param status  处理结果，2:成功3失败
     * @param fileId  文件id
     * @param url 文件访问url
     * @param errorMsg 失败原因
     * @return void
     * @author Mr.M
     * @date 2022/9/14 14:45
     */
    @Transactional
    public void saveProcessFinishStatus(int status,String fileId, String url,String errorMsg);

    /**
     * @description 获取待处理任务
     * @param shardIndex 分片序号
     * @param shardTotal 分片总数
     * @param count 获取记录数
     * @return java.util.List<com.xuecheng.media.model.po.MediaProcess>
     * @author Mr.M
     * @date 2022/9/14 14:49
     */
    public List<MediaProcess> getMediaProcessList(int shardIndex, int shardTotal, int count);

    /**
     * @description 保存进程完成状态
     * @param taskId 任务id
     * @param status 状态
     * @param fileId 文件id
     * @param url url地址
     * @param errorMsg 错误信息
     *
     * @author LGY
     * @date 2023/02/16 16:09
     */
    void saveProcessFinishStatus(Long taskId, String status, String fileId, String url, String errorMsg);
}
