package com.xuecheng.media.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.media.model.po.MqMessageHistory;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author itcast
 * @since 2023-02-13
 */
public interface MqMessageHistoryService extends IService<MqMessageHistory> {

}
