package com.xuecheng.content;

import com.xuecheng.content.service.TeachplanService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;

import java.io.IOException;

@SpringBootTest
class XuechengPlusContentApplicationTests {

    @Test
    void contextLoads() throws IOException, InterruptedException {

        Process exec = Runtime.getRuntime().exec("D:/App/CloudMusic/cloudmusic.exe");

//        ProcessBuilder builder = new ProcessBuilder();
//        builder.command();
////将标准输入流和错误输入流合并，通过标准输入流程读取信息
//        builder.redirectErrorStream(true);
//        Process p = builder.start();
    }

    @Autowired
    TeachplanService teachplanService;

    @Test
    void teachPlanTreeNodes(){
        StopWatch stopWatch = new StopWatch();
        StopWatch stopWatch2 = new StopWatch();
        stopWatch.start();
        teachplanService.findTeachplayTree(117);
        stopWatch.stop();
        stopWatch2.start();
        teachplanService.findTeachplayTree(117);
        stopWatch2.stop();
        System.err.println("1:" + stopWatch.prettyPrint());
        System.err.println("2" + stopWatch2.prettyPrint());
    }

    @Test
    void teachPlanTreeNodes2(){

//        Long i = 0L;
//        System.out.println(i.equals(0L));
        teachplanService.findTeachplayTree2(117);

    }

}
