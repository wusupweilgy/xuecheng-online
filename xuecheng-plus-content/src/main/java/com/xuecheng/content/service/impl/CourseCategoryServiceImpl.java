package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xuecheng.content.mapper.CourseCategoryMapper;
import com.xuecheng.content.model.dto.CourseCategoryTreeDto;
import com.xuecheng.content.model.po.CourseCategory;
import com.xuecheng.content.service.CourseCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 课程类别服务
 * <p>
 * 课程分类 服务实现类
 * </p>
 *
 * @author LGY
 * @date 2023/02/08
 */
@Slf4j
@Service
public class CourseCategoryServiceImpl extends ServiceImpl<CourseCategoryMapper, CourseCategory> implements CourseCategoryService {

    @Autowired
    CourseCategoryMapper courseCategoryMapper;

    @Override
    public List<CourseCategoryTreeDto> queryTreeNodes() {
        return courseCategoryMapper.selectTreeNodes();
    }

    @Override
    public List<CourseCategory> queryTreeNodes2() {

        List<CourseCategory> list = courseCategoryMapper.selectList(null);
        List<CourseCategory> collect = list.stream()
                .filter(item -> item.getParentid().equals("1"))
                .map(menu -> {
                    menu.setChildrenTreeNodes(getChildrens(menu, list));
                    return menu;
                }).sorted((menu1, menu2) -> {

                    return (menu1.getOrderby() == null ? 0 : menu1.getOrderby()) - (menu2.getOrderby() == null ? 0 : menu2.getOrderby());

                }).collect(Collectors.toList());
        return  collect;
    }

    private List<CourseCategory> getChildrens(CourseCategory menu, List<CourseCategory> list) {
        List<CourseCategory> childrens = list.stream().filter(item -> {
            return item.getParentid().equals(menu.getId());
        }).map(item -> {
            item.setChildrenTreeNodes(getChildrens(item, list));
            return item;
        }).sorted((menu1, menu2) -> {
            return (menu1.getOrderby() == null ? 0 : menu1.getOrderby()) - (menu2.getOrderby() == null ? 0 : menu2.getOrderby());
        }).collect(Collectors.toList());

        return childrens;
    }
}
