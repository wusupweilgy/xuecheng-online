package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xuecheng.content.mapper.TeachplanMapper;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;
import com.xuecheng.content.model.po.TeachplanMedia;
import com.xuecheng.content.service.TeachplanMediaService;
import com.xuecheng.content.service.TeachplanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 课程计划 服务实现类
 * </p>
 *
 * @author LGY
 */
@Slf4j
@Service
public class TeachplanServiceImpl extends ServiceImpl<TeachplanMapper, Teachplan> implements TeachplanService {


    @Autowired
    TeachplanMapper teachplanMapper;

    @Autowired
    TeachplanMediaService teachplanMediaService;

    @Autowired
    TeachplanService teachplanService;

    @Override
    public List<TeachplanDto> findTeachplayTree(long courseId) {
        return teachplanMapper.selectTreeNodes(courseId);
    }

    @Override
    public List<Teachplan> findTeachplayTree2(long courseId) {

        LambdaQueryWrapper<Teachplan> wq = new LambdaQueryWrapper<>();
        wq.eq(Teachplan::getCourseId,courseId);
        List<Teachplan> teachplans = teachplanMapper.selectList(wq);
        List<Teachplan> collect = teachplans.stream()
                .filter(item -> item.getParentid().equals(0L))
                .map(menu -> {
                    menu.setTeachPlanTreeNodes(getChildrens(menu,teachplans));
                    return menu;
                }).sorted((menu1, menu2) -> {
                    return (menu1.getOrderby() == null ? 0 : menu1.getOrderby()) - (menu2.getOrderby() == null ? 0 : menu2.getOrderby());
                }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public void saveTeachplan(SaveTeachplanDto teachplanDto) {

        Long id = teachplanDto.getId();
        Teachplan teachplan = new Teachplan();
        BeanUtils.copyProperties(teachplanDto,teachplan);

        int count = countTeachplan(teachplanDto.getCourseId(),teachplanDto.getParentid());
        teachplan.setOrderby(count+1);
        teachplanService.saveOrUpdate(teachplan);
    }

    private int countTeachplan(Long courseId, Long parentid) {
        LambdaQueryWrapper<Teachplan> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Teachplan::getCourseId,courseId);
        wrapper.eq(Teachplan::getParentid,parentid);
        Integer count = teachplanMapper.selectCount(wrapper);
        return count;
    }


    private List<Teachplan> getChildrens(Teachplan menu, List<Teachplan> teachplans) {
        List<Teachplan> childrens = teachplans.stream()
                .filter(item -> item.getParentid().equals(menu.getId()))
                .map(item -> {
                    TeachplanMedia media = teachplanMediaService.getOne(new LambdaQueryWrapper<TeachplanMedia>().eq(TeachplanMedia::getTeachplanId, item.getId()).last("limit 1"));
                    item.setTeachplanMedia(media);
                    item.setTeachPlanTreeNodes(getChildrens(item,teachplans));
                    return item;
                }).sorted((menu1, menu2) -> {
                    return (menu1.getOrderby() == null ? 0 : menu1.getOrderby()) - (menu2.getOrderby() == null ? 0 : menu2.getOrderby());
                }).collect(Collectors.toList());
        return childrens;
    }
}
