package com.xuecheng.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.po.TeachplanMedia;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LGY
 * @since 2023-01-19
 */
public interface TeachplanMediaService extends IService<TeachplanMedia> {


    /**
     * @description 教学计划板顶媒资
     * @param bindTeachplanMediaDto
     * @return com.xuecheng.content.model.po.TeachplanMedia
     * @author Mr.M
     * @date 2022/9/14 22:20
     */
    public TeachplanMedia associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto);

    /**
     * @description 删除教学计划与媒资之间的绑定关系
     * @param teachPlanId  教学计划id
     * @param mediaId  媒资文件id
     * @return void
     * @author Mr.M
     * @date 2022/9/14 22:21
     */
    public void delAassociationMedia(Long teachPlanId, String mediaId);
}
