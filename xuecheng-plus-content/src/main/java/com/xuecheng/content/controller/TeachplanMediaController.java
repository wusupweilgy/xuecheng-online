package com.xuecheng.content.controller;

import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.service.TeachplanMediaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "计划和媒资关系管理接口",tags = "计划和媒资关系管理接口")
@RestController
public class TeachplanMediaController {

    @Autowired
    TeachplanMediaService teachplanMediaService;

    @ApiOperation(value = "课程计划和媒资信息绑定")
    @PostMapping("/teachplan/association/media")
    void associationMedia(@RequestBody BindTeachplanMediaDto bindTeachplanMediaDto){
        teachplanMediaService.associationMedia(bindTeachplanMediaDto);
    }

    @ApiOperation(value = "课程计划和媒资信息解除绑定")
    @DeleteMapping("/teachplan/association/media/{teachPlanId}/{mediaId}")
    void delAssociationMedia(@PathVariable Long teachPlanId,@PathVariable String mediaId){
        teachplanMediaService.delAassociationMedia(teachPlanId,mediaId);
    }
}
