package com.xuecheng.content.model.dto;


import com.xuecheng.content.model.po.Teachplan;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @description 课程预览数据模型
 * @author Mr.M
 * @date 2022/9/16 15:03
 * @version 1.0
 */
@Data
@ToString
@ApiModel(value = "EditCourse",description = "课程营销信息")
public class CoursePreviewDto {

    //课程基本信息,课程营销信息
    CourseBaseInfoDto courseBase;


    //课程计划信息
    List<Teachplan> teachplans;

    //师资信息暂时不加...


}