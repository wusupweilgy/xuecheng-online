package com.xuecheng.content.model.dto;

import com.xuecheng.content.model.po.CourseCategory;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @author LGY
 * @version 1.0.0
 * @description 课程类别树dto
 * @date 2023/01/22 22:10:24
 */
@Data
public class CourseCategoryTreeDto extends CourseCategory implements Serializable {

    List childrenTreeNodes;
}
